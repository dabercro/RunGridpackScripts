#! /bin/bash

./submit.sh 1 testconfig 40

if [ ! -d test ]
then
    mkdir test
fi

cp dojob.sh test/.

cd test

export LSB_MAX_NUM_PROCESSORS=1
export LS_SUBCWD=".."
export LSB_QUEUE="testconfig"

sed -i 's@&& xrdcp@#&& xrdcp@g' dojob.sh
sed -i 's@xrdcp@#xrdcp@g' dojob.sh

./dojob.sh 1 40
