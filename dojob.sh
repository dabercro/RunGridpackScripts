#! /bin/bash

##################################
# A couple of functions we'll use

setup_CMSSW () {           # Create CMSSW directory and cmsenv

    name=CMSSW_$1

    if [ ! -d $name ]
    then

        scramv1 project CMSSW $name

    fi

    cd $name/src
    eval `scramv1 runtime -sh`
    cd ../../

}

setOutput () {             # Set variables for names

    lastOut=$outputName
    outputName=output_$1.root
    pythonName=python_$1.py

}

###################################
# Here is the script doing the job

seed=$1

if [ "$seed" = "" ]         # If just sourcing for functions, exit
then

    return 0

fi

nevents=$2

export X509_USER_PROXY=$HOME/private/personal/voms_proxy.cert

started=`date +%y%m%d`

ls

source  /afs/cern.ch/cms/cmsset_default.sh

# Generate the LHE file:

export SCRAM_ARCH=slc6_amd64_gcc481

setup_CMSSW 7_1_19

if [ ! -f runcmsgrid.sh ]
then

    cp /afs/cern.ch/work/d/dabercro/public/VAJetsToQQ/VAJetsToQQ_012j_LO_MLM_tarball.tar.xz .
    tar -xf VAJetsToQQ_012j_LO_MLM_tarball.tar.xz

fi

if [ ! -f cmsgrid_final.lhe ]
then

    ./runcmsgrid.sh $nevents $seed $LSB_MAX_NUM_PROCESSORS

fi

ls

# Generate GENSIM

setOutput gensim
cmsRun $LS_SUBCWD/$LSB_QUEUE/$pythonName && lastOut=$outputName

# Generate AODSIM

export SCRAM_ARCH=slc6_amd64_gcc530

setup_CMSSW 8_0_3_patch1

setOutput rawsim
cmsRun $LS_SUBCWD/$LSB_QUEUE/$pythonName && lastOut=$outputName

setOutput aodsim
cmsRun $LS_SUBCWD/$LSB_QUEUE/$pythonName && lastOut=$outputName

# Generate MINIAODSIM

setup_CMSSW 8_0_5_patch1

setOutput miniaod
cmsRun $LS_SUBCWD/$LSB_QUEUE/$pythonName && lastOut=$outputName

# Create NeroNtuples

if [ "$lastOut" = "$outputName" ]
then

    xrdcp $lastOut root://eoscms.cern.ch//store/user/dabercro/VA_gensim/$started/VA_`date +%y%m%d_%H%M`_$seed\_$lastOut

    # Make Nero Ntuples

    theDir=`pwd`
    cd /afs/cern.ch/user/d/dabercro/public/Fall16/CMSSW_8_0_12/src/NeroProducer/Nero/test
    eval `scramv1 runtime -sh`
    cmsRun testNero.py inputFile=$theDir/output_miniaod.root outputFile=$theDir/NeroNtuples.root && lastOut=NeroNtuples.root
    cd $theDir

fi

# Copy output

copyTo=VA_`date +%y%m%d_%H%M`_$seed\_$lastOut
echo "Copying $lastOut to $started/$copyTo"

xrdcp $lastOut root://eoscms.cern.ch//store/user/dabercro/VA_privateprod/$started/$copyTo
