#! /bin/bash

max=250
queue=2nw4cores
nevents=10000

if [ $1 != "" ]
then

    max=$1
    shift

fi

if [ $1 != "" ]
then

    queue=$1
    shift

fi

if [ $1 != "" ]
then

    nevents=$1
    shift

fi

if [ "$queue" = "2nw4cores" ]
then

    ncores=4

else

    ncores=1

fi

if [ ! -d $queue/bout ]
then

    mkdir -p $queue/bout

fi

echo ""
echo "Jobs:    $max"
echo "Queue:   $queue"
echo "NEvents: $nevents"
echo "NCores:  $ncores"
echo ""

# Get a couple of functions from dojob.sh

. dojob.sh

# Generate the gensim config

export SCRAM_ARCH=slc6_amd64_gcc481

setup_CMSSW 7_1_19

setOutput gensim

command="cmsDriver.py Configuration/Generator/python/Hadronizer_TuneCUETP8M1_13TeV_MLM_5f_max4j_LHE_pythia8_cff.py --filein file:cmsgrid_final.lhe --fileout file:$outputName --mc --eventcontent RAWSIM --customise SLHCUpgradeSimulations/Configuration/postLS1Customs.customisePostLS1,Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM --conditions MCRUN2_71_V1::All --beamspot Realistic50ns13TeVCollision --step GEN,SIM --magField 38T_PostLS1 -n $nevents --python_filename $queue/$pythonName --no_exec"
echo $command
$command

# Generate AODSIM configs

export SCRAM_ARCH=slc6_amd64_gcc530

setup_CMSSW 8_0_3_patch1

setOutput rawsim

command="cmsDriver.py step1 --filein file:$lastOut --fileout file:$outputName --pileup_input dbs:/MinBias_TuneCUETP8M1_13TeV-pythia8/RunIISummer15GS-MCRUN2_71_V1-v2/GEN-SIM --mc --eventcontent RAWSIM --pileup 2016_25ns_SpringMC_PUScenarioV1_PoissonOOTPU --datatier GEN-SIM-RAW --conditions 80X_mcRun2_asymptotic_2016_v3 --step DIGI,L1,DIGI2RAW,HLT:@frozen25ns --era Run2_25ns --customise Configuration/DataProcessing/Utils.addMonitoring --nThreads $ncores -n $nevents --python_filename $queue/$pythonName --no_exec"
echo $command
$command

setOutput aodsim

command="cmsDriver.py step2 --filein file:$lastOut --fileout file:$outputName --mc --eventcontent AODSIM,DQM --runUnscheduled --datatier AODSIM,DQMIO --conditions 80X_mcRun2_asymptotic_2016_v3 --step RAW2DIGI,L1Reco,RECO,EI,DQM:DQMOfflinePOGMC --era Run2_25ns --customise Configuration/DataProcessing/Utils.addMonitoring --nThreads $ncores -n $nevents --python_filename $queue/$pythonName --no_exec"
echo $command
$command

# Generate MINIAOD config

setup_CMSSW 8_0_5_patch1

setOutput miniaod

command="cmsDriver.py step1 --filein file:$lastOut --fileout file:$outputName --mc --eventcontent MINIAODSIM --runUnscheduled --datatier MINIAODSIM --conditions 80X_mcRun2_asymptotic_2016_miniAODv2_v0 --step PAT --era Run2_25ns --customise Configuration/DataProcessing/Utils.addMonitoring --nThreads $ncores -n $nevents --python_filename $queue/$pythonName --no_exec"
echo $command
$command

for seed in $(seq 1 1 $max)
do

    command="bsub -q $queue -n $ncores -o $queue/bout/out.%J dojob.sh $seed $nevents"
    echo $command
    $command

done
